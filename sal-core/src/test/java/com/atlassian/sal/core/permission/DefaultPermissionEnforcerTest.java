package com.atlassian.sal.core.permission;

import com.atlassian.sal.api.permission.AuthorisationException;
import com.atlassian.sal.api.permission.NotAuthenticatedException;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.isA;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DefaultPermissionEnforcerTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private UserKey userKey;
    @Mock
    private UserManager userManager;
    private DefaultPermissionEnforcer permissionEnforcer;

    @Before
    public void setup() {
        permissionEnforcer = new DefaultPermissionEnforcer(userManager);
        userKey = new UserKey("1234");
    }

    @Test
    public void testEnforceAdminWithAdminPermission() {
        when(userManager.getRemoteUserKey()).thenReturn(userKey);
        when(userManager.isAdmin(userKey)).thenReturn(true);

        permissionEnforcer.enforceAdmin();
        verify(userManager).isAdmin(userKey);
    }

    @Test
    public void testEnforceAdminThrowsIfNotAdmin() {
        thrown.expect(AuthorisationException.class);
        thrown.expect(not(isA(NotAuthenticatedException.class)));
        when(userManager.getRemoteUserKey()).thenReturn(userKey);

        permissionEnforcer.enforceAdmin();
    }

    @Test
    public void testEnforceAdminThrowsIfNotAuthenticated() {
        thrown.expect(NotAuthenticatedException.class);

        permissionEnforcer.enforceAdmin();
    }

    @Test
    public void testEnforceAuthenticatedWhenAuthenticated() {
        when(userManager.getRemoteUserKey()).thenReturn(userKey);

        permissionEnforcer.enforceAuthenticated();
        verify(userManager).getRemoteUserKey();
    }

    @Test
    public void testEnforceAuthenticatedThrowsIfNotAuthenticated() {
        thrown.expect(AuthorisationException.class);

        permissionEnforcer.enforceAuthenticated();
    }

    @Test
    public void testEnforceSystemAdminWithSystemAdminPermission() {
        when(userManager.getRemoteUserKey()).thenReturn(userKey);
        when(userManager.isSystemAdmin(userKey)).thenReturn(true);

        permissionEnforcer.enforceSystemAdmin();
        verify(userManager).isSystemAdmin(userKey);
    }

    @Test
    public void testEnforceSystemAdminThrowsIfNotSystemAdmin() {
        thrown.expect(AuthorisationException.class);
        thrown.expect(not(isA(NotAuthenticatedException.class)));
        when(userManager.getRemoteUserKey()).thenReturn(userKey);

        permissionEnforcer.enforceSystemAdmin();
    }

    @Test
    public void testEnforceSystemAdminThrowsIfNotAuthenticated() {
        thrown.expect(NotAuthenticatedException.class);

        permissionEnforcer.enforceSystemAdmin();
    }

    @Test
    public void testIsAdminWithAdminPermission() {
        when(userManager.getRemoteUserKey()).thenReturn(userKey);
        when(userManager.isAdmin(userKey)).thenReturn(true);

        assertTrue(permissionEnforcer.isAdmin());
    }

    @Test
    public void testIsAdminIfNotAdmin() {
        when(userManager.getRemoteUserKey()).thenReturn(userKey);

        assertFalse(permissionEnforcer.isAdmin());
    }

    @Test
    public void testIsAdminIfNotAuthenticated() {
        assertFalse(permissionEnforcer.isAdmin());
    }

    @Test
    public void testIsAuthenticatedWhenAuthenticated() {
        when(userManager.getRemoteUserKey()).thenReturn(userKey);

        assertTrue(permissionEnforcer.isAuthenticated());
    }

    @Test
    public void testIsAuthenticatedWhenNotAuthenticated() {
        assertFalse(permissionEnforcer.isAuthenticated());
    }

    @Test
    public void testIsSystemAdminWithSystemAdminPermission() {
        when(userManager.getRemoteUserKey()).thenReturn(userKey);
        when(userManager.isSystemAdmin(userKey)).thenReturn(true);

        assertTrue(permissionEnforcer.isSystemAdmin());
    }

    @Test
    public void testIsSystemAdminIfNotAdmin() {
        when(userManager.getRemoteUserKey()).thenReturn(userKey);

        assertFalse(permissionEnforcer.isSystemAdmin());
    }

    @Test
    public void testIsSystemAdminIfNotAuthenticated() {
        assertFalse(permissionEnforcer.isSystemAdmin());
    }
}