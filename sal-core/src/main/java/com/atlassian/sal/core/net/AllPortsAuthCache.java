package com.atlassian.sal.core.net;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScheme;
import org.apache.http.client.AuthCache;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static com.atlassian.sal.core.util.Assert.notNull;

/**
 * An AuthCache implementation allowing setting Auth for a host on all ports
 *
 * When getting a value for a host, we try first to find exact match and then
 * a match for all ports
 *
 * @since v3.0
 */
class AllPortsAuthCache implements AuthCache {
    private final Map<HttpHost, AuthScheme> map = new ConcurrentHashMap<>();

    @Override
    public AuthScheme get(final HttpHost host) {
        notNull(host, "HTTP host");
        // looking for exact match
        AuthScheme authScheme = map.get(host);

        if (authScheme != null) {
            return authScheme;
        }

        // Exact match not found, looking for all ports match
        return map.get(new HttpHost(host.getHostName()));
    }

    @Override
    public void put(final HttpHost host, final AuthScheme authScheme) {
        notNull(host, "HTTP host");
        this.map.put(host, authScheme);
    }

    @Override
    public void remove(final HttpHost host) {
        notNull(host, "HTTP host");
        this.map.remove(host);
    }

    @Override
    public void clear() {
        this.map.clear();
    }

    @Override
    public String toString() {
        return this.map.toString();
    }
}
