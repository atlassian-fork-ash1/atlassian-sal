package com.atlassian.sal.core.net;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ProxyUtilTest {
    @Test
    public void requiresAuthentication() {
        assertFalse(ProxyUtil.requiresAuthentication(getConfig(null, null, new String[0]),
                "http://localhost:6990/bamboo"));
        assertTrue(ProxyUtil.requiresAuthentication(getConfig("admin", "admin", new String[0]),
                "http://localhost:6990/bamboo"));
        assertFalse(ProxyUtil.requiresAuthentication(getConfig("admin", "admin", new String[]{"localhost"}),
                "http://localhost:6990/bamboo"));
        assertTrue(ProxyUtil.requiresAuthentication(getConfig("admin", "admin", new String[]{"localhost"}),
                "http://bamboo.localhost:6990/bamboo"));
        assertTrue(ProxyUtil.requiresAuthentication(getConfig("admin", "admin", new String[]{"localhost"}),
                "http://@#$@#$:6990/bamboo"));
        assertFalse(ProxyUtil.requiresAuthentication(getConfig("admin", "admin", new String[]{"localhost"}),
                "http://:6990/bamboo"));
        assertFalse(ProxyUtil.requiresAuthentication(getConfig("admin", "admin", new String[]{"localhost", "127.0.0.1"}),
                "http://localhost:6990/bamboo"));
        assertFalse(ProxyUtil.requiresAuthentication(getConfig("admin", "admin", new String[]{"127.0.0.1", "*.localhost"}),
                "http://user.localhost:6990/bamboo"));
        assertFalse(ProxyUtil.requiresAuthentication(getConfig("admin", "admin", new String[]{"*localhost"}),
                "http://user.localhost:6990/bamboo"));
        assertFalse(ProxyUtil.requiresAuthentication(getConfig("admin", "admin", new String[]{"127.0.*", "*.localhost"}),
                "http://127.0.0.1:6990/bamboo"));
        assertFalse(ProxyUtil.requiresAuthentication(getConfig("admin", "admin", new String[]{"127.0.0.*"}),
                "http://127.0.0.1:6990/bamboo"));
    }

    @Test
    public void literal_IPV6_address_match() {
        assertFalse(ProxyUtil.requiresAuthentication(getConfig("admin", "admin", new String[]{"[::1]"}),
                "http://[::1]/bamboo"));
        assertTrue(ProxyUtil.requiresAuthentication(getConfig("admin", "admin", new String[]{"[::1]"}),
                "http://[::1:1]/bamboo"));
    }

    private ProxyConfig getConfig(final String user, final String password, final String[] nonProxyHosts) {
        return new ProxyConfig() {
            @Override
            public boolean isSet() {
                return true;
            }

            @Override
            public boolean requiresAuthentication() {
                return getUser() != null;
            }

            @Override
            public String getHost() {
                return null;
            }

            @Override
            public int getPort() {
                return 0;
            }

            @Override
            public String getUser() {
                return user;
            }

            @Override
            public String getPassword() {
                return password;
            }

            @Override
            public String[] getNonProxyHosts() {
                return nonProxyHosts;
            }
        };
    }
}
