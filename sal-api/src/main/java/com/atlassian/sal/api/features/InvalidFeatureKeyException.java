package com.atlassian.sal.api.features;

/**
 * @since 2.10
 */
public class InvalidFeatureKeyException extends IllegalArgumentException {
    public InvalidFeatureKeyException(String message) {
        super(message);
    }
}
