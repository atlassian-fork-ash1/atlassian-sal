package com.atlassian.sal.api.events;

import javax.annotation.Nullable;

import static com.google.common.base.Preconditions.checkNotNull;

public abstract class AbstractSessionEvent {
    private static final String SESSION_ID_NULL_MSG = "Session ID must be supplied";

    protected final String sessionId;
    protected final String userName;

    protected AbstractSessionEvent(final String sessionId, final String userName) {
        this.sessionId = checkNotNull(sessionId, SESSION_ID_NULL_MSG);
        this.userName = userName;
    }

    public String getSessionId() {
        return sessionId;
    }

    @Nullable
    public String getUserName() {
        return userName;
    }

    public abstract static class Builder {
        protected String sessionId;
        protected String userName;

        protected Builder() {
        }

        public Builder sessionId(final String sessionId) {
            this.sessionId = checkNotNull(sessionId, SESSION_ID_NULL_MSG);
            return this;
        }

        public Builder userName(@Nullable final String userName) {
            this.userName = userName;
            return this;
        }

        public abstract AbstractSessionEvent build();
    }
}
