package com.atlassian.sal.api.rdbms;

import com.atlassian.annotations.PublicApi;

import java.sql.Connection;

/**
 * Callback to be executed by {@link com.atlassian.sal.api.rdbms.TransactionalExecutor#execute(ConnectionCallback)}
 *
 * @since 3.0
 */
@PublicApi
public interface ConnectionCallback<A> {
    /**
     * Execute a method that uses the <code>connection</code> passed.
     * <p>
     * See {@link com.atlassian.sal.api.rdbms.TransactionalExecutor} for allowed actions.
     *
     * @param connection guaranteed to be provided
     * @return optional, may be {@link java.lang.Void}
     */
    A execute(Connection connection);
}
