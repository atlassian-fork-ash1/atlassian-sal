package com.atlassian.sal.core.message;

import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.message.Message;
import org.junit.Test;

import java.io.Serializable;
import java.util.Locale;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class TestAbstractI18nResolver {
    private I18nResolver assertingResolver = new AbstractI18nResolver() {
        @Override
        public String resolveText(String key, Serializable[] arguments) {
            assertEquals(0, arguments.length);
            return "";
        }

        @Override
        public String resolveText(Locale locale, String key, Serializable[] arguments) {
            assertEquals(0, arguments.length);
            return "";
        }

        public Map<String, String> getAllTranslationsForPrefix(String prefix) {
            throw new UnsupportedOperationException();
        }

        public Map<String, String> getAllTranslationsForPrefix(String prefix, Locale locale) {
            throw new UnsupportedOperationException();
        }

        public String getRawText(final String key) {
            throw new UnsupportedOperationException();
        }

        public String getRawText(final Locale locale, final String key) {
            throw new UnsupportedOperationException();
        }
    };

    @Test
    public void testGetTextWithOnlyKeyParameter() {
        assertingResolver.getText("hello world");
    }

    @Test
    public void testGetTextWithMessageParameterWithZeroArgument() {
        Message message = new DefaultMessage("fun");
        assertingResolver.getText(message);
    }

    @Test(expected = NullPointerException.class)
    public void testGetTextWithLocaleRequiresNonNullLocale() {
        assertingResolver.getText(null, "hello world");
    }

    @Test
    public void testGetTextWithLocaleAndKey() {
        assertingResolver.getText(Locale.ENGLISH, "hello world");
    }
}
