package com.atlassian.sal.api.page;

/**
 * Login page capabilities, implementation should provide a page which matches behavior/look requirements from those capabilities.
 */
public enum PageCapability {
    /**
     * Page should be suitable for presentation inside iframe HTML
     */
    IFRAME
}
