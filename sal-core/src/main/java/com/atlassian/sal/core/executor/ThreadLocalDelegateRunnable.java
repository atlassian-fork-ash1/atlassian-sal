package com.atlassian.sal.core.executor;

import com.atlassian.sal.api.executor.ThreadLocalContextManager;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A delegating runnable that copies the thread local state into the executing thread.
 *
 * @since 2.0
 */
class ThreadLocalDelegateRunnable<C> implements Runnable {
    private final C context;
    private final Runnable delegate;
    private final ThreadLocalContextManager<C> manager;
    private final ClassLoader contextClassLoader;

    /**
     * @param manager  The manager to get the context from
     * @param delegate The runnable to delegate to
     */
    ThreadLocalDelegateRunnable(ThreadLocalContextManager<C> manager, Runnable delegate) {
        this.delegate = checkNotNull(delegate);
        this.manager = checkNotNull(manager);
        this.context = manager.getThreadLocalContext();
        this.contextClassLoader = Thread.currentThread().getContextClassLoader();
    }

    public void run() {
        ClassLoader oldContextClassLoader = Thread.currentThread().getContextClassLoader();
        try {
            Thread.currentThread().setContextClassLoader(contextClassLoader);
            manager.setThreadLocalContext(context);
            delegate.run();
        } finally {
            Thread.currentThread().setContextClassLoader(oldContextClassLoader);
            manager.clearThreadLocalContext();
        }
    }
}
