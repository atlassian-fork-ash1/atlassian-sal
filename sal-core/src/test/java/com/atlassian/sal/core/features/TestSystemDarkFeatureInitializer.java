package com.atlassian.sal.core.features;

import org.junit.Rule;
import org.junit.Test;

import java.net.URL;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestSystemDarkFeatureInitializer {
    @Rule
    public final ClearSystemPropertyRule systemProperty = new ClearSystemPropertyRule();

    private final URL resource = getClass().getClassLoader().getResource("testdarkfeatures.properties");

    @Test
    public void testNoFileDarkFeatures() {
        final SystemDarkFeatureInitializer.SystemDarkFeatures systemDarkFeatures = SystemDarkFeatureInitializer.getSystemStartupDarkFeatures();

        assertTrue(systemDarkFeatures.getEnabled().isEmpty());
        assertTrue(systemDarkFeatures.getDisabled().isEmpty());
        assertFalse(systemDarkFeatures.isDisableAll());
    }

    @Test
    public void testSystemPropsDarkFeatures() {
        systemProperty.setProperty("atlassian.darkfeature.foo", "true");
        systemProperty.setProperty("atlassian.darkfeature.bar", "false");
        systemProperty.setProperty("atlassian.darkfeature.baz", "maybe");
        systemProperty.setProperty("atlassian.darkfeatures.fake", "ignored");

        final SystemDarkFeatureInitializer.SystemDarkFeatures systemDarkFeatures = SystemDarkFeatureInitializer.getSystemStartupDarkFeatures();
        final Set<String> enabledSystemDarkFeatures = systemDarkFeatures.getEnabled();
        final Set<String> disabledSystemDarkFeatures = systemDarkFeatures.getDisabled();

        assertEquals(1, enabledSystemDarkFeatures.size());
        assertEquals(1, disabledSystemDarkFeatures.size());
        assertFalse(systemDarkFeatures.isDisableAll());

        assertTrue(enabledSystemDarkFeatures.contains("foo"));
        assertTrue(disabledSystemDarkFeatures.contains("bar"));
    }

    @Test
    public void testPropertiesFileDarkFeatures() {
        systemProperty.setProperty(DefaultDarkFeatureManager.DARKFEATURES_PROPERTIES_FILE_PROPERTY, resource.getPath());

        final SystemDarkFeatureInitializer.SystemDarkFeatures systemDarkFeatures = SystemDarkFeatureInitializer.getSystemStartupDarkFeatures();
        final Set<String> enabledSystemDarkFeatures = systemDarkFeatures.getEnabled();
        final Set<String> disabledSystemDarkFeatures = systemDarkFeatures.getDisabled();

        assertEquals(1, enabledSystemDarkFeatures.size());
        assertEquals(1, disabledSystemDarkFeatures.size());
        assertFalse(systemDarkFeatures.isDisableAll());

        assertTrue(enabledSystemDarkFeatures.contains("foo"));
        assertTrue(disabledSystemDarkFeatures.contains("bar"));
    }

    @Test
    public void testDarkFeaturesFromBothSources() {
        systemProperty.setProperty("atlassian.darkfeature.bat", "true");
        systemProperty.setProperty("atlassian.darkfeature.bam", "false");
        systemProperty.setProperty("atlassian.darkfeature.bax", "maybe");

        systemProperty.setProperty(DefaultDarkFeatureManager.DARKFEATURES_PROPERTIES_FILE_PROPERTY, resource.getPath());

        final SystemDarkFeatureInitializer.SystemDarkFeatures systemDarkFeatures = SystemDarkFeatureInitializer.getSystemStartupDarkFeatures();
        final Set<String> enabledSystemDarkFeatures = systemDarkFeatures.getEnabled();
        final Set<String> disabledSystemDarkFeatures = systemDarkFeatures.getDisabled();

        assertEquals(2, enabledSystemDarkFeatures.size());
        assertEquals(2, disabledSystemDarkFeatures.size());
        assertFalse(systemDarkFeatures.isDisableAll());

        assertTrue(enabledSystemDarkFeatures.contains("foo"));
        assertTrue(enabledSystemDarkFeatures.contains("bat"));
        assertTrue(disabledSystemDarkFeatures.contains("bar"));
        assertTrue(disabledSystemDarkFeatures.contains("bam"));
    }

    @Test
    public void testContradictingDarkFeatures() {
        // test that in the case of collisions, system properties trump contents of dark features properties file.
        systemProperty.setProperty("atlassian.darkfeature.foo", "false");
        systemProperty.setProperty("atlassian.darkfeature.bar", "true");
        systemProperty.setProperty("atlassian.darkfeature.bat", "true");

        systemProperty.setProperty(DefaultDarkFeatureManager.DARKFEATURES_PROPERTIES_FILE_PROPERTY, resource.getPath());

        final SystemDarkFeatureInitializer.SystemDarkFeatures systemDarkFeatures = SystemDarkFeatureInitializer.getSystemStartupDarkFeatures();
        final Set<String> enabledSystemDarkFeatures = systemDarkFeatures.getEnabled();
        final Set<String> disabledSystemDarkFeatures = systemDarkFeatures.getDisabled();

        assertEquals(2, enabledSystemDarkFeatures.size());
        assertEquals(1, disabledSystemDarkFeatures.size());
        assertFalse(systemDarkFeatures.isDisableAll());

        // system properties should trump contents of testdarkFeatures.properties
        assertTrue(enabledSystemDarkFeatures.contains("bar"));
        assertTrue(enabledSystemDarkFeatures.contains("bat"));
        assertTrue(disabledSystemDarkFeatures.contains("foo"));
    }

    @Test
    public void testDisableAll() {
        systemProperty.setProperty(DefaultDarkFeatureManager.DISABLE_ALL_DARKFEATURES_PROPERTY, "true");
        systemProperty.setProperty(DefaultDarkFeatureManager.DARKFEATURES_PROPERTIES_FILE_PROPERTY, resource.getPath());

        final SystemDarkFeatureInitializer.SystemDarkFeatures systemDarkFeatures = SystemDarkFeatureInitializer.getSystemStartupDarkFeatures();

        assertTrue(systemDarkFeatures.isDisableAll());
        assertTrue(systemDarkFeatures.getEnabled().isEmpty());
    }

}

