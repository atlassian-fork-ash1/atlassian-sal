package com.atlassian.sal.core.net;

public interface ProxyConfig {
    boolean isSet();

    boolean requiresAuthentication();

    String getHost();

    int getPort();

    String getUser();

    String getPassword();

    String[] getNonProxyHosts();

}
