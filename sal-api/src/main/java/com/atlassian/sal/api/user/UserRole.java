package com.atlassian.sal.api.user;

/**
 * Enforce certain level of privileges on log in page
 */
public enum UserRole {
    USER,
    ADMIN,
    SYSADMIN
}
