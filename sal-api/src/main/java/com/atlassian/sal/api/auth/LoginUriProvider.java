package com.atlassian.sal.api.auth;

import com.atlassian.sal.api.page.PageCapability;
import com.atlassian.sal.api.user.UserRole;

import java.net.URI;
import java.util.EnumSet;

/**
 * Provides the {@code URI} to redirect users to for them to login before they can
 * authorize consumer requests to access their data.
 *
 * @since 2.0
 */
public interface LoginUriProvider {

    /**
     * Returns the {@code URI} to redirect users for login.  It must append the {@code returnUri}
     * so that once login is complete, the user will be redirected back to the original page.
     *
     * @param returnUri {@code URI} of the page the application should redirect the user to after login is complete
     * @return the {@code URI} to redirect users for login
     * @throws RuntimeException if the returnUri is not a valid URI, or cannot be url encoded properly
     */
    URI getLoginUri(URI returnUri);

    /**
     * Returns the {@code URI} to redirect users for login.  It must append the {@code returnUri}
     * so that once login is complete, the user will be redirected back to the original page.
     *
     * @param returnUri {@code URI} of the page the application should redirect the user to after login is complete
     * @param pageCaps  {@code PageCapability} for product to supply in login page.
     * @return the {@code URI} to redirect users for login
     * @throws RuntimeException if the returnUri is not a valid URI, or cannot be url encoded properly
     */
    URI getLoginUri(URI returnUri, EnumSet<PageCapability> pageCaps);

    /**
     * Returns the {@code URI} to redirect users for login.  It must append the {@code returnUri}
     * so that once login is complete, the user will be redirected back to the original page.
     *
     * Page is expected to redirect back to {@code URI} only if u
     *
     * @param returnUri {@code URI} of the page the application should redirect the user to after login is complete
     * @param role      {@code UserRole} of user
     * @return the {@code URI} to redirect users for login
     * @throws RuntimeException if the returnUri is not a valid URI, or cannot be url encoded properly
     */
    URI getLoginUriForRole(URI returnUri, UserRole role);

    /**
     * Returns the {@code URI} to redirect users for login.  It must append the {@code returnUri}
     * so that once login is complete, the user will be redirected back to the original page.
     *
     * Page is expected to redirect back to {@code URI} only if u
     *
     * This page is supposed to match requested {@code PageCapability}
     *
     * @param returnUri {@code URI} of the page the application should redirect the user to after login is complete
     * @param role      {@code UserRole} of user
     * @param pageCaps  {@code PageCapability} for product to supply in login page.
     * @return the {@code URI} to redirect users for login
     * @throws RuntimeException if the returnUri is not a valid URI, or cannot be url encoded properly
     */
    URI getLoginUriForRole(URI returnUri, UserRole role, EnumSet<PageCapability> pageCaps);
}
