package com.atlassian.sal.api.features;

import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.junit.Test;

import static com.atlassian.sal.api.features.FeatureKeyScope.ALL_USERS;
import static com.atlassian.sal.api.features.FeatureKeyScope.ALL_USERS_READ_ONLY;
import static com.atlassian.sal.api.features.FeatureKeyScope.CURRENT_USER_ONLY;
import static com.atlassian.sal.api.features.FeatureKeyScopePredicate.filterBy;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class TestEnabledDarkFeatures {
    private static final String FEATURE_KEY = "feature";
    private static final String ANOTHER_FEATURE_KEY = "another-feature";

    @Test
    public void unmodifiableFeaturesEnabledForAllUsers() {
        final EnabledDarkFeatures enabledDarkFeatures = createEnabledDarkFeatures(ALL_USERS_READ_ONLY);
        assertTrue(enabledDarkFeatures.isFeatureEnabled(FEATURE_KEY));
        assertThat(enabledDarkFeatures.getFeatureKeys(), containsInAnyOrder(FEATURE_KEY));
        assertThat(enabledDarkFeatures.getFeatureKeys(filterBy(ALL_USERS_READ_ONLY)), containsInAnyOrder(FEATURE_KEY));
    }

    @Test
    public void modifiableFeaturesEnabledForAllUser() {
        final EnabledDarkFeatures enabledDarkFeatures = createEnabledDarkFeatures(ALL_USERS);
        assertTrue(enabledDarkFeatures.isFeatureEnabled(FEATURE_KEY));
        assertThat(enabledDarkFeatures.getFeatureKeys(), containsInAnyOrder(FEATURE_KEY));
        assertThat(enabledDarkFeatures.getFeatureKeys(filterBy(ALL_USERS)), containsInAnyOrder(FEATURE_KEY));
    }

    @Test
    public void featuresEnabledForAllUsers() {
        final EnabledDarkFeatures enabledDarkFeatures = createEnabledDarkFeatures(ALL_USERS);
        assertThat(enabledDarkFeatures.getFeatureKeys(Predicates.<FeatureKeyScope>or(filterBy(ALL_USERS_READ_ONLY), filterBy(ALL_USERS))), containsInAnyOrder(FEATURE_KEY));
    }

    @Test
    public void featuresEnabledForCurrentUser() {
        final EnabledDarkFeatures enabledDarkFeatures = createEnabledDarkFeatures(CURRENT_USER_ONLY);
        assertTrue(enabledDarkFeatures.isFeatureEnabled(FEATURE_KEY));
        assertThat(enabledDarkFeatures.getFeatureKeys(), containsInAnyOrder(FEATURE_KEY));
        assertThat(enabledDarkFeatures.getFeatureKeys(filterBy(CURRENT_USER_ONLY)), containsInAnyOrder(FEATURE_KEY));
    }

    @Test
    public void featureEnabledForAllAndPerUser() {
        final EnabledDarkFeatures enabledDarkFeatures = createEnabledDarkFeatureInTwoScopes(ALL_USERS_READ_ONLY, CURRENT_USER_ONLY);
        assertTrue(enabledDarkFeatures.isFeatureEnabled(FEATURE_KEY));
        assertThat(enabledDarkFeatures.getFeatureKeys(), containsInAnyOrder(FEATURE_KEY));
        assertThat(enabledDarkFeatures.getFeatureKeys(filterBy(ALL_USERS_READ_ONLY)), containsInAnyOrder(FEATURE_KEY));
        assertThat(enabledDarkFeatures.getFeatureKeys(filterBy(CURRENT_USER_ONLY)), containsInAnyOrder(FEATURE_KEY));
    }

    @Test
    public void multipleFeaturesEnabledForAllUsers() {
        final EnabledDarkFeatures enabledDarkFeatures = createTwoEnabledDarkFeatures(ALL_USERS_READ_ONLY);
        assertTrue(enabledDarkFeatures.isFeatureEnabled(FEATURE_KEY));
        assertTrue(enabledDarkFeatures.isFeatureEnabled(ANOTHER_FEATURE_KEY));
        assertThat(enabledDarkFeatures.getFeatureKeys(), containsInAnyOrder(FEATURE_KEY, ANOTHER_FEATURE_KEY));
    }

    private EnabledDarkFeatures createEnabledDarkFeatures(final FeatureKeyScope featureKeyScope) {
        return new EnabledDarkFeatures(ImmutableMap.<FeatureKeyScope, ImmutableSet<String>>of(featureKeyScope, ImmutableSet.of(FEATURE_KEY)));
    }

    private EnabledDarkFeatures createTwoEnabledDarkFeatures(final FeatureKeyScope featureKeyScope) {
        return new EnabledDarkFeatures(ImmutableMap.<FeatureKeyScope, ImmutableSet<String>>of(featureKeyScope, ImmutableSet.of(FEATURE_KEY, ANOTHER_FEATURE_KEY)));
    }

    private EnabledDarkFeatures createEnabledDarkFeatureInTwoScopes(final FeatureKeyScope featureKeyScope1, final FeatureKeyScope featureKeyScope2) {
        return new EnabledDarkFeatures(ImmutableMap.<FeatureKeyScope, ImmutableSet<String>>of(
                featureKeyScope1, ImmutableSet.of(FEATURE_KEY),
                featureKeyScope2, ImmutableSet.of(FEATURE_KEY)
        ));
    }
}
