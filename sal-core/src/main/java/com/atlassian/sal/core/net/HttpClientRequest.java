package com.atlassian.sal.core.net;

import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.RequestFilePart;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseHandler;
import com.atlassian.sal.api.net.ResponseProtocolException;
import com.atlassian.sal.api.net.ResponseStatusException;
import com.atlassian.sal.api.net.ReturningResponseHandler;
import com.google.common.base.Preconditions;
import org.apache.http.HttpHost;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * HttpClient implementation of Request interface
 */
public class HttpClientRequest<T extends Request<?, ?>, RESP extends Response> implements Request<HttpClientRequest<?, ?>, HttpClientResponse> {
    private static final Logger log = LoggerFactory.getLogger(HttpClientRequest.class);

    private final CloseableHttpClient httpClient;
    private final List<NameValuePair> requestParameters;
    protected final HttpClientContext httpClientContext;
    final RequestBuilder requestBuilder;
    final RequestConfig.Builder requestConfigBuilder;

    // Unfortunately we need to keep a list of the headers
    private final Map<String, List<String>> headers = new HashMap<>();

    public HttpClientRequest(CloseableHttpClient httpClient, HttpClientContext httpClientContext, MethodType initialMethodType, String initialUrl) {
        this.httpClient = httpClient;
        this.httpClientContext = httpClientContext;
        this.requestBuilder = RequestBuilder.create(initialMethodType.toString()).setUri(initialUrl);
        this.requestParameters = new LinkedList<>();

        final ConnectionConfig connectionConfig = new SystemPropertiesConnectionConfig();
        this.requestConfigBuilder = RequestConfig.custom()
                .setConnectTimeout(connectionConfig.getConnectionTimeout())
                .setSocketTimeout(connectionConfig.getSocketTimeout())
                .setMaxRedirects(connectionConfig.getMaxRedirects())
                .setCookieSpec(CookieSpecs.STANDARD);
    }

    @Override
    public String execute() throws ResponseException {
        return executeAndReturn(response -> {
            if (!response.isSuccessful()) {
                throw new ResponseStatusException("Unexpected response received. Status code: " + response.getStatusCode(),
                        response);
            }
            return response.getResponseBodyAsString();
        });
    }

    @Override
    public void execute(final ResponseHandler<? super HttpClientResponse> responseHandler) throws ResponseException {
        executeAndReturn((ReturningResponseHandler<HttpClientResponse, Void>) response -> {
            responseHandler.handle(response);
            return null;
        });
    }

    @Override
    public <RET> RET executeAndReturn(final ReturningResponseHandler<? super HttpClientResponse, RET> responseHandler)
            throws ResponseException {
        if (!requestParameters.isEmpty()) {
            requestBuilder.setEntity(new UrlEncodedFormEntity(requestParameters, StandardCharsets.UTF_8));
        }
        final HttpUriRequest request = requestBuilder.setConfig(requestConfigBuilder.build()).build();
        log.debug("Executing request:{}", request);

        try (final CloseableHttpResponse response = httpClient.execute(request, httpClientContext)) {
            return responseHandler.handle(new HttpClientResponse(response));
        } catch (ClientProtocolException cpe) {
            throw new ResponseProtocolException(cpe);
        } catch (IOException e) {
            throw new ResponseException(e);
        }
    }

    @Override
    public Map<String, List<String>> getHeaders() {
        return Collections.unmodifiableMap(headers);
    }

    @Override
    public HttpClientRequest addBasicAuthentication(final String hostname, final String username, final String password) {
        httpClientContext.getCredentialsProvider().setCredentials(
                new AuthScope(hostname, AuthScope.ANY_PORT),
                new UsernamePasswordCredentials(username, password));
        httpClientContext.getAuthCache().put(new HttpHost(hostname), new BasicScheme());
        return this;
    }

    @Override
    public HttpClientRequest setConnectionTimeout(final int connectionTimeout) {
        requestConfigBuilder.setConnectionRequestTimeout(connectionTimeout);
        return this;
    }

    @Override
    public HttpClientRequest setSoTimeout(final int soTimeout) {
        requestConfigBuilder.setSocketTimeout(soTimeout);
        return this;
    }

    @Override
    public HttpClientRequest setUrl(final String url) {
        requestBuilder.setUri(url);
        return this;
    }

    @Override
    public HttpClientRequest setRequestBody(final String requestBody) {
        return setRequestBody(requestBody, ContentType.TEXT_PLAIN.getMimeType());
    }

    @Override
    public HttpClientRequest setRequestBody(final String requestBodyString, final String contentTypeString) {
        Preconditions.checkNotNull(requestBodyString);
        Preconditions.checkNotNull(contentTypeString);
        Preconditions.checkState(isRequestBodyMethod(), "Only PUT or POST methods accept a request body.");

        requestBuilder.setEntity(new StringEntity(requestBodyString, ContentType.create(contentTypeString, StandardCharsets.UTF_8)));
        return this;
    }

    @Override
    public HttpClientRequest setFiles(final List<RequestFilePart> requestBodyFiles) {
        Preconditions.checkNotNull(requestBodyFiles);
        Preconditions.checkState(isRequestBodyMethod(), "Only PUT or POST methods accept a request body.");

        final MultipartEntityBuilder multipartEntityBuilder = MultipartEntityBuilder.create();

        for (RequestFilePart requestBodyFile : requestBodyFiles) {
            final ContentType fileContentType = ContentType.create(requestBodyFile.getContentType());
            multipartEntityBuilder.addBinaryBody(requestBodyFile.getParameterName(), requestBodyFile.getFile(), fileContentType, requestBodyFile.getFileName());
        }

        requestBuilder.setEntity(multipartEntityBuilder.build());
        return this;
    }

    @Override
    public HttpClientRequest addRequestParameters(final String... params) {
        Preconditions.checkNotNull(params);
        Preconditions.checkState(isRequestBodyMethod(), "Only PUT or POST methods accept a request body.");

        if (params.length % 2 != 0) {
            throw new IllegalArgumentException("You must enter an even number of arguments.");
        }

        for (int i = 0; i < params.length; i += 2) {
            final String name = params[i];
            final String value = params[i + 1];
            requestParameters.add(new BasicNameValuePair(name, value));
        }

        return this;
    }

    private boolean isRequestBodyMethod() {
        final String methodType = requestBuilder.getMethod();
        return HttpPost.METHOD_NAME.equals(methodType) || HttpPut.METHOD_NAME.equals(methodType);
    }

    @Override
    public HttpClientRequest addHeader(final String headerName, final String headerValue) {
        headers.computeIfAbsent(headerName, k -> new ArrayList<>())
                .add(headerValue);
        requestBuilder.addHeader(headerName, headerValue);
        return this;
    }

    @Override
    public HttpClientRequest setHeader(final String headerName, final String headerValue) {
        headers.put(headerName, new ArrayList<>(Collections.singletonList(headerValue)));
        requestBuilder.setHeader(headerName, headerValue);
        return this;
    }

    @Override
    public HttpClientRequest setFollowRedirects(final boolean follow) {
        requestConfigBuilder.setRedirectsEnabled(follow);
        return this;
    }

    @Override
    public HttpClientRequest setEntity(final Object entity) {
        throw new UnsupportedOperationException("This SAL request does not support object marshalling. Use the RequestFactory component instead.");
    }
}
