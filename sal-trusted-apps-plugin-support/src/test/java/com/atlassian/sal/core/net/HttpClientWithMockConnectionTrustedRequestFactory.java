package com.atlassian.sal.core.net;

import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.core.trusted.CertificateFactory;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.protocol.HttpRequestExecutor;

public class HttpClientWithMockConnectionTrustedRequestFactory extends HttpClientTrustedRequestFactory {
    private final HttpClientConnectionManager mockConnectionManager;
    private final HttpRequestExecutor mockRequestExecutor;

    public HttpClientWithMockConnectionTrustedRequestFactory(final UserManager userManager, final CertificateFactory certificateFactory, final HttpClientConnectionManager mockConnectionManager, final HttpRequestExecutor mockRequestExecutor) {
        super(userManager, certificateFactory);
        this.mockConnectionManager = mockConnectionManager;
        this.mockRequestExecutor = mockRequestExecutor;
    }

    protected HttpRequestExecutor getRequestExecutor() {
        return mockRequestExecutor;
    }

    /**
     * This mocks out the connection, so we don't actually need a running server
     *
     * @return
     */
    @Override
    protected HttpClientConnectionManager getConnectionManager() {
        return mockConnectionManager;
    }


}
