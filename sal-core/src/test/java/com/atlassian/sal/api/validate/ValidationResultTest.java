package com.atlassian.sal.api.validate;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class ValidationResultTest {
    @Test
    public void testNoDuplicateMessages() {
        List<String> messages = Lists.newArrayList();
        messages.add("Message One");
        messages.add("Message Two");
        messages.add("Message Duplicate");
        messages.add("Message Duplicate");
        final ValidationResult validationResult = ValidationResult.withErrorAndWarningMessages(messages, messages);
        assertEquals(3, Iterables.size(validationResult.getErrorMessages()));
        assertEquals(3, Iterables.size(validationResult.getWarningMessages()));
    }

    @Test
    public void testImmutable() {
        List<String> messages = Lists.newArrayList();
        messages.add("Message One");
        messages.add("Message Two");
        messages.add("Message Tree");
        final ValidationResult validationResult = ValidationResult.withErrorAndWarningMessages(messages, messages);
        messages.add("Another message");
        assertEquals(3, Iterables.size(validationResult.getErrorMessages()));
        assertEquals(3, Iterables.size(validationResult.getWarningMessages()));
    }

    @Test
    public void testWaringAndError() {
        List<String> errorMessages = Lists.newArrayList();
        errorMessages.add("ERROR One");
        errorMessages.add("ERROR Two");
        errorMessages.add("ERROR Tree");

        List<String> warningMessages = Lists.newArrayList();
        warningMessages.add("WARN One");
        warningMessages.add("WARN Two");
        warningMessages.add("WARN Tree");
        warningMessages.add("WARN Four");

        final ValidationResult validationResult = ValidationResult.withErrorAndWarningMessages(errorMessages, warningMessages);
        assertTrue(validationResult.hasErrors());
        assertTrue(validationResult.hasWarnings());
        assertFalse(validationResult.isValid());
        assertEquals(3, Iterables.size(validationResult.getErrorMessages()));
        assertEquals(4, Iterables.size(validationResult.getWarningMessages()));
        assertThat(validationResult.getErrorMessages(), contains(errorMessages.toArray(new String[errorMessages.size()])));
        assertThat(validationResult.getWarningMessages(), contains(warningMessages.toArray(new String[warningMessages.size()])));
    }

    @Test
    public void testWaring() {
        List<String> warningMessages = Lists.newArrayList();
        warningMessages.add("WARN One");
        warningMessages.add("WARN Two");
        warningMessages.add("WARN Tree");
        warningMessages.add("WARN Four");

        final ValidationResult validationResult = ValidationResult.withWarningMessages(warningMessages);
        assertFalse(validationResult.hasErrors());
        assertTrue(validationResult.hasWarnings());
        assertTrue(validationResult.isValid());
        assertNotNull(validationResult.getErrorMessages());
        assertNotNull(validationResult.getWarningMessages());
        assertEquals(0, Iterables.size(validationResult.getErrorMessages()));
        assertEquals(4, Iterables.size(validationResult.getWarningMessages()));
        assertThat(validationResult.getWarningMessages(), contains(warningMessages.toArray(new String[warningMessages.size()])));
    }

    @Test
    public void testErrors() {
        List<String> errorMessages = Lists.newArrayList();
        errorMessages.add("ERROR One");
        errorMessages.add("ERROR Two");
        errorMessages.add("ERROR Tree");

        final ValidationResult validationResult = ValidationResult.withErrorMessages(errorMessages);
        assertTrue(validationResult.hasErrors());
        assertFalse(validationResult.hasWarnings());
        assertFalse(validationResult.isValid());
        assertNotNull(validationResult.getErrorMessages());
        assertNotNull(validationResult.getWarningMessages());
        assertEquals(3, Iterables.size(validationResult.getErrorMessages()));
        assertEquals(0, Iterables.size(validationResult.getWarningMessages()));
        assertThat(validationResult.getErrorMessages(), contains(errorMessages.toArray(new String[errorMessages.size()])));
    }

    @Test(expected = NullPointerException.class)
    public void testNullErrors() {
        ValidationResult.withErrorMessages(null);
    }

    @Test(expected = NullPointerException.class)
    public void testNullWarning() {
        ValidationResult.withWarningMessages(null);
    }

    @Test(expected = NullPointerException.class)
    public void testWithNullWarningsButErrors() {
        ValidationResult.withErrorAndWarningMessages(Lists.<String>newArrayList(), null);
    }

    @Test(expected = NullPointerException.class)
    public void testWithNullErrorsButWarnings() {
        ValidationResult.withErrorAndWarningMessages(null, Lists.<String>newArrayList());
    }
}