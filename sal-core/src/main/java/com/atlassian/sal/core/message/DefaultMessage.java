package com.atlassian.sal.core.message;

import com.atlassian.sal.api.message.Message;

import java.io.Serializable;
import java.util.Arrays;

public class DefaultMessage implements Message {
    private final Serializable[] arguments;
    private String key;

    public DefaultMessage(String key, Serializable... arguments) {
        this.key = key;
        this.arguments = arguments;
    }

    public Serializable[] getArguments() {
        return arguments;
    }

    public String getKey() {
        return key;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(key);
        builder.append(": ");
        for (Serializable argument : arguments) {
            builder.append(argument);
            builder.append(",");
        }
        return builder.toString();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DefaultMessage)) {
            return false;
        }

        final DefaultMessage that = (DefaultMessage) o;

        if (!Arrays.equals(arguments, that.arguments)) {
            return false;
        }
        if (key != null ? !key.equals(that.key) : that.key != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = arguments != null ? Arrays.hashCode(arguments) : 0;
        result = 31 * result + (key != null ? key.hashCode() : 0);
        return result;
    }
}

