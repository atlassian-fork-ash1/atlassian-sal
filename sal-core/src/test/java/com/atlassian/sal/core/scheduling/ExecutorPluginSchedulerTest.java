package com.atlassian.sal.core.scheduling;

import com.atlassian.sal.api.scheduling.PluginJob;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class ExecutorPluginSchedulerTest {

    private ScheduledExecutorService executor;
    private ExecutorPluginScheduler scheduler;

    @Before
    public void setUp() {
        executor = Executors.newScheduledThreadPool(1);
        scheduler = new ExecutorPluginScheduler(executor);
    }

    @After
    public void tearDown() {
        executor.shutdown();
    }

    @Test
    public void testSchedule() throws Exception {
        scheduler.scheduleJob("test1", TestPluginJob.class, Collections.emptyMap(), new Date(), 60000);
        waitForCondition(input -> input.get() == 1, 180000);
        scheduler.unscheduleJob("test1");
        assertJobIsNotScheduled("test1");

        scheduler.scheduleJob("test2", TestPluginJob.class, Collections.emptyMap(), new Date(), 500);
        waitForCondition(input -> input.get() > 1, 180000);
        scheduler.unscheduleJob("test2");
        assertJobIsNotScheduled("test2");
    }

    @Test
    public void testUnscheduleInexisting() {
        assertJobIsNotScheduled("inexistingjob");
    }

    private void waitForCondition(Predicate<AtomicInteger> condition, int timeoutInMs) throws InterruptedException {
        long start = System.currentTimeMillis();
        while (!condition.test(TestPluginJob.executionCount) && (System.currentTimeMillis() - start < timeoutInMs)) {
            Thread.sleep(500);
        }
    }

    private void assertJobIsNotScheduled(String jobKey) {
        try {
            scheduler.unscheduleJob(jobKey);
            fail("IllegalArgumentException should have thrown");
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().contains("unknown job"));
        }
    }

    public static class TestPluginJob implements PluginJob {
        private static final AtomicInteger executionCount = new AtomicInteger(0);

        @Override
        public void execute(Map<String, Object> jobDataMap) {
            executionCount.incrementAndGet();
        }
    }
}
