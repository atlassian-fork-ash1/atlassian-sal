package com.atlassian.sal.core.rdbms;

import io.atlassian.fugue.Option;
import com.atlassian.sal.api.rdbms.ConnectionCallback;
import com.atlassian.sal.api.rdbms.RdbmsException;
import com.atlassian.sal.spi.HostConnectionAccessor;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.sql.Connection;
import java.sql.SQLException;

import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestDefaultTransactionalExecutor {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private DefaultTransactionalExecutor transactionalExecutor;

    @Mock
    private HostConnectionAccessor hostConnectionAccessor;
    @Mock
    private Connection connection;
    @Mock
    private ConnectionCallback<Object> callback;
    @Mock
    private Object result;

    private WrappedConnection wrappedConnection;

    private Throwable callbackThrows;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        transactionalExecutor = new DefaultTransactionalExecutor(hostConnectionAccessor, false, false);

        callbackThrows = null;

        when(hostConnectionAccessor.execute(Mockito.anyBoolean(), Mockito.anyBoolean(), any())).thenAnswer(i -> {
            ConnectionCallback callback = (ConnectionCallback) i.getArguments()[2];
            return callback.execute(connection);
        });

        // grab a hold of the wrapped connection each time
        when(callback.execute(any(WrappedConnection.class))).thenAnswer(i -> {
            wrappedConnection = (WrappedConnection) i.getArguments()[0];
            if (callbackThrows != null) {
                throw callbackThrows;
            } else {
                return result;
            }
        });
    }

    @Test
    public void testGetSchemaName() {
        when(hostConnectionAccessor.getSchemaName()).thenReturn(Option.option("schema"));

        assertThat(transactionalExecutor.getSchemaName().get(), is("schema"));
    }

    @Test
    public void testChangeProperties() {
        transactionalExecutor.readOnly();
        assertThat(transactionalExecutor.readOnly, is(true));

        transactionalExecutor.readWrite();
        assertThat(transactionalExecutor.readOnly, is(false));

        transactionalExecutor.newTransaction();
        assertThat(transactionalExecutor.newTransaction, is(true));

        transactionalExecutor.existingTransaction();
        assertThat(transactionalExecutor.newTransaction, is(false));
    }

    @Test
    public void testExecute() throws SQLException {
        assertThat(transactionalExecutor.execute(callback), is(result));
        assertThat(wrappedConnection.connection, nullValue());

        verify(callback).execute(any(WrappedConnection.class));
        verify(connection, never()).commit();
        verify(connection, never()).rollback();
    }

    @Test
    public void testExecuteFailsOnAutoCommit() throws SQLException {
        when(connection.getAutoCommit()).thenReturn(true);

        expectedException.expect(IllegalStateException.class);

        transactionalExecutor.execute(callback);
    }

    @Test
    public void testExecuteAutoCommitFails() throws SQLException {
        SQLException exception = new SQLException("horrible getAutoCommit error");
        doThrow(exception).when(connection).getAutoCommit();

        expectedException.expect(RdbmsException.class);
        expectedException.expectCause(is(exception));

        transactionalExecutor.execute(callback);
        verify(callback, never()).execute(any(WrappedConnection.class));
    }

    @Test
    public void testExecuteFailsOnAutoCommitGetFail() throws SQLException {
        when(connection.getAutoCommit()).thenThrow(new SQLException("horrible getAutoCommit exception"));

        expectedException.expect(RdbmsException.class);

        transactionalExecutor.execute(callback);
    }
}
