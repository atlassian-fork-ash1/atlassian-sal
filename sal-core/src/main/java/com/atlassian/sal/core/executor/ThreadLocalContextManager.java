package com.atlassian.sal.core.executor;

/**
 * Manager for retrieving and storing a single object which represents all thread local state
 *
 * @deprecated use {@link com.atlassian.sal.api.executor.ThreadLocalContextManager} instead
 */
@Deprecated
public interface ThreadLocalContextManager extends com.atlassian.sal.api.executor.ThreadLocalContextManager<Object> {
}
